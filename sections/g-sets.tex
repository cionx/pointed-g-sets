\chapter{\texorpdfstring{$G$}{G}-sets}





\section{Basic notions}



\subsection{\texorpdfstring{$G$}{G}-sets}

\begin{convention}
	In the following,~$G$ denotes a group.
\end{convention}

\begin{definition}
	Let~$X$ be a set.
	An \defemph{action} of~$G$ on~$X$ is a map
	\[
		α
		\colon
		G × X \to X \,,
		\quad
		(g, x) \mapsto g \act x
	\]
	such that for any two group elements~$g$ and~$h$ of~$G$ and every element~$x$ of~$X$,
	\[
		g \act (h \act x)
		=
		(gh) \act x \,,
	\]
	and for every element~$x$ of~$X$,
	\[
		1 \act x = x \,.
	\]
\end{definition}

\begin{definition}
	A~\set{$G$} is a pair~$(X, α)$ consisting of a set~$X$ and an action of~$α$ on~$X$.
\end{definition}

\begin{remark}
	We will typically refrain from giving the action an explicit name, and simply write~$g \act x$ for the action of a group element~$g$ of the~$G$ on an element~$x$ of the set~$X$.
\end{remark}

\begin{lemma}
	Let~$X$ be a~\set{$G$}.
	Every group element~$g$ of the~$G$ results in a self-bijection of the set~$X$ given by
	\[
		X \to X \,,
		\quad
		g \mapsto g \act x \,.
	\]
\end{lemma}

\begin{proposition}
	Let~$X$ be a set.
	\begin{enumerate}

		\item
			Every action~$G$ on~$X$ induces a homomorphism of groups from~$G$ to~$\symm(X)$ given by
			\[
				G \to \symm(X) \,,
				\quad
				g \mapsto α(g, \ph) \,.
			\]

		\item
			Every homomorphism of groups~$ρ$ from~$G$ to~$\symm(X)$ induces an action of~$G$ on~$X$ given by
			\[
				G × X \to X \,,
				\quad
				(g, x) \mapsto ρ(g)(x) \,.
			\]

		\item
			The above two constructions are mutually inverse and result in a one-to-one correspondence
			\[
				\{
					\text{actions of~$G$ on~$X$}
				\}
				\onetoone
				\{
					\text{homomorphisms of groups from~$G$ to~$\symm(X)$}
				\} \,.
			\]

	\end{enumerate}
\end{proposition}



\subsection{Equivariant maps}

\begin{definition}
	Let~$X$ and~$Y$ be two~\sets{$G$}.
	A map~$f$ from~$X$ to~$Y$ is~\defemph{\equivariant{$G$}} if
	\[
		φ(g \act x) = g \act φ(x)
		\qquad
		\text{for all~$g ∈ G$,~$x ∈ X$}.
	\]
\end{definition}

\begin{proposition}
	Let~$X$,~$Y$ and~$Z$ be~\sets{$G$}.
	\begin{enumerate}

		\item
			The identity map of~$X$ is~\equivariant{$G$}.

		\item
			Let~$φ$ be a~\equivariant{$G$} map from~$X$ to~$Y$ and let~$ψ$ be a~\equivariant{$G$} map from~$Y$ to~$Z$.
			The composite~$ψ ∘ φ$ is a~\equivariant{$G$} from~$X$ to~$Z$.

	\end{enumerate}
\end{proposition}

\begin{corollary}
	The class of~\sets{$G$} together with the~\equivariant{$G$} maps between them form a concrete category.
\end{corollary}

\begin{notation}
	We denote the category of~\sets{$G$} by~$\Set[G]$.
\end{notation}

\begin{remark}
	We have a forgetful functor from~$\Set[G]$ to~$\Set$ that assigns to each~\set{$G$} its underlying set.
\end{remark}





\section{Categorical constructions}



\subsection{\texorpdfstring{$G$}{G}-subsets}

\begin{definition}
	Let~$X$ be a \set{$G$}.
	A subset~$Y$ of~$X$ is a~\defemph{\subset{$G$}} if it is closed under the action of~$G$ on~$X$:
	the element~$g \act y$ is again contained in~$Y$ for every group element~$g$ of~$G$ and every element~$y$ of~$Y$.
\end{definition}

\begin{remark}
	Let~$X$ be a~\set{$G$} and let~$Y$ be a~\subset{$G$} of~$X$.
	We can restrict the action of~$G$ on~$X$ to an action of~$G$ on~$Y$, which makes the set~$Y$ into a~\set{$G$}.
	We will always regard~\subsets{$G$} as~\sets{$G$} in this way.
\end{remark}

\begin{proposition}
	Let~$X$ be a~\set{$G$} and let~$Y$ be a~\subset{$G$} of~$X$.
	The inclusion map from~$X$ to~$Y$ is~\equivariant{$G$}.
\end{proposition}

\begin{remark}
	Let~$X$ be a~\set{$G$}.
	\begin{enumerate}

		\item
			A~\set{$G$}~$Y$ is a~\subset{$G$} of~$X$ if and only if~$Y$ is a subset of~$X$ and~$g \act_X y = g \act_Y y$ for every group element~$g$ of~$G$ and every element~$y$ of~$Y$.

		\item
			A~\set{$G$}~$Y$ is a~\subset{$G$} of~$X$ if and only if~$Y$ is a subset of~$X$ and the inclusion map from~$Y$ to~$X$ is~\equivariant{$G$}.

	\end{enumerate}
\end{remark}

\begin{proposition}[Universal property of~\subsets{$G$}]
	Let~$X$ be~\set{$G$} and let~$Y$ be a~\subset{$G$} of~$X$.
	Let~$ι$ be the inclusion map from~$X$ to~$Y$.
	\begin{enumerate}

		\item
			Let~$Z$ be another~\set{$X$}.
			A map~$φ$ from~$Z$ to~$Y$ is~\equivariant{$G$} if and only if the composite~$φ ∘ ι$ is~\equivariant{$G$}.

		\item
			The~\equivariant{$G$} map~$ι$ induces for every~\set{$G$}~$Z$ a natural bijection
			\begin{align*}
				\{
					\text{\equivariant{$G$} maps~$φ \colon Z \to Y$}
				\}
				&\onetoone
				\left\{
					\begin{tabular}{@{}l@{}}
						\equivariant{$G$} maps~$ψ \colon Z \to X$ \\
						with~$ψ(Z) ⊆ Y$
					\end{tabular}
				\right\} \,,
				\\
				φ &\mapsto φ ∘ ι \,,
				\\
				\restrict{ψ}{}[Y] &\mapsfrom ψ \,.
			\end{align*}

	\end{enumerate}
\end{proposition}

\begin{proposition}
	Let~$X$ and~$Y$ be two a~\set{$G$} and let~$φ$ be a~\equivariant{$G$} map from~$X$ to~$Y$.
	The image of~$φ$ is a~\subset{$G$} of~$Y$.
\end{proposition}



\subsection{Quotients}

\begin{definition}
	Let~$X$ be a~\set{$G$}.
	An equivalence relation on~$X$ is a \defemph{congruence relation} if it follows for any two elements~$x_1$ and~$x_2$ of~$X$ from~$x_1 ∼ x_2$ that more generally~$g \act x_1 ∼ g \act x_2$ for every group element~$g$ of~$G$.
\end{definition}

\begin{lemma}
	Let~$X$ be a~\set{$G$}.
	An equivalence relation~$∼$ on~$X$ is a congruence relation if and only if the map
	\[
		G / {∼} \to G / {∼} \,,
		\quad
		\class{x} \mapsto \class{g \act x}
	\]
	is well-defined for every group element~$g$ of~$G$.
\end{lemma}

\begin{proposition}
	\label{construction_of_quotient_g_sets}
	Let~$X$ be a~\set{$G$} and let~$∼$ be a congruence relation on~$X$.
	The map
	\[
		G × (X / {∼}) \to X / {∼} \,,
		\quad
		(g, \class{x}) \mapsto \class{g \act x}
	\]
	is a well-defined action of~$G$ on~$X / {∼}$.
\end{proposition}

\begin{definition}
	In the situation of \cref{construction_of_quotient_g_sets}, the~\set{$G$}~$X / {∼}$ is the \defemph{quotient} of~$X$ by~$∼$.
\end{definition}



\subsection{Products}

\begin{proposition}
	\label{construction_of_product_g_sets}
	Let~$(X_λ)_{λ ∈ Λ}$ be a family of~\sets{$G$}.
	The group~$G$ acts on the product~$∏_{λ ∈ Λ} X_λ$ via
	\[
		g \act (x_λ)_λ ≔ (g \act x_λ)_λ
	\]
	for every group element~$g$ of~$G$ and every element~$(x_λ)_λ$ of~$∏_{λ ∈ Λ} X_λ$.
\end{proposition}

\begin{definition}
	In the situation of \cref{construction_of_product_g_sets}, the~\set{$G$}~$∏_{λ ∈ Λ} X_λ$ is the \defemph{product} of the~\sets{$G$}~$X_λ$.
\end{definition}

\begin{proposition}
	Let~$(X_λ)_{λ ∈ Λ}$ be a family of~\sets{$G$}.
	The canonical projection map
	\[
		∏_{λ ∈ Λ} X_λ \to X_μ \,,
		\quad
		(x_λ)_λ \mapsto x_μ
	\]
	is~\equivariant{$G$} for every index~$μ$ in~$Λ$.
\end{proposition}

\begin{proposition}[Universal property of the product]
	Let~$(X_λ)_{λ ∈ Λ}$ be a family of~\sets{$G$}, and for every index~$μ$ in~$Λ$ let~$π_μ$ be the canonical projection map from~$\prod_{λ ∈ Λ} X_λ$ to~$X_μ$.
	\begin{enumerate}

		\item
			Let~$Z$ be another~\set{$G$}.
			A map~$φ$ from~$Z$ to~$\prod_{λ ∈ Λ} X_λ$ is~\equivariant{$G$} if and only of the composite~$π_μ ∘ φ$ is~\equivariant{$G$} for every index~$μ$ in~$Λ$.

		\item
			The family~$(π_μ)_μ$ of~\equivariant{$G$} maps~$π_μ$ from~$\prod_{λ ∈ Λ} X_λ$ to~$X_μ$ induces for every~\set{$G$}~$Z$ a natural one-to-one correspondence
			\begin{align*}
				\Biggl\{
					\text{\equivariant{$G$} maps~$φ \colon Z \to ∏_{λ ∈ Λ} X_λ$}
				\Biggr\}
				&\onetoone
				\left\{
					\begin{tabular}{@{}l@{}}
						families~$(ψ_λ)_λ$ of \\
						\equivariant{$G$} maps~$ψ_λ$ from~$Z$ to~$X_λ$
					\end{tabular}
				\right\} \,,
				\\
				φ
				&\mapsto (π_λ ∘ φ)_{λ ∈ Λ} \,,
				\\
				[ z \textstyle\mapsto (ψ_λ(z))_{λ ∈ Λ} ]
				&\mapsfrom
				(ψ_λ)_λ \,.
			\end{align*}

	\end{enumerate}
\end{proposition}



\subsection{Disjoint unions}

\begin{proposition}
	\label{construction_of_coproduct_g_sets}
	Let~$(X_λ)_{λ ∈ Λ}$ be a family of~\sets{$G$}.
	The group~$G$ acts again on the disjoint union~$∐_{λ ∈ Λ} X_λ$ via
	\[
		g \act (x, λ) ≔ (g \act x, λ)
	\]
	for every group element~$g$ of~$G$, every index~$λ$ in~$Λ$ and every element~$x$ of~$X_λ$.
\end{proposition}

\begin{definition}
	In the situation of \cref{construction_of_coproduct_g_sets}, the~\set{$G$}~$∐_{λ ∈ Λ} X_λ$ is the \defemph{disjoint union} of the~\sets{$G$}~$X_λ$.
\end{definition}

\begin{proposition}
	Let~$(X_λ)_{λ ∈ Λ}$ be a family of~\sets{$G$}.
	The canonical inclusion map
	\[
		X_μ \to ∐_{λ ∈ Λ} X_λ \,,
		\quad
		x \mapsto (x, μ)
	\]
	is~\equivariant{$G$} for every index~$μ$ in~$Λ$.
\end{proposition}

\begin{proposition}[Universal property of the disjoint union]
	Let~$(X_λ)_λ$ be a family of~\sets{$G$}, and for every index~$μ$ in~$Λ$ let~$ι_μ$ be the canonical inclusion map from~$X_μ$ to~$∐_{λ ∈ Λ} X_λ$.
	\begin{enumerate}

		\item
			Let~$Y$ be another~\set{$G$} and let~$φ$ be a map from~$∐_{λ ∈ Λ} X_λ$ to~$Y$.
			The map~$φ$ is~\equivariant{$G$} if and only if the composite~$φ ∘ ι_μ$ is~\equivariant{$G$} for every index~$μ$ in~$Λ$.

		\item
			The family of~\equivariant{$G$} maps~$(ι_λ)_{λ ∈ Λ}$ induces for every~\set{$G$}~$Y$ a natural one-to-one correspondence
			\begin{align*}
				\Biggl\{
					\text{\equivariant{$G$} maps~$φ \colon ∐_{λ ∈ Λ} X_λ \to Y$}
				\Biggr\}
				&\onetoone
				\left\{
					\begin{tabular}{@{}l@{}}
						families~$(ψ_λ)_{λ ∈ Λ}$ of \\
						\equivariant{$G$} maps~$ψ_λ \colon X_λ \to Y$
					\end{tabular}
				\right\} \,,
				\\
				φ
				&\mapsto
				(φ ∘ ι_λ)_{λ ∈ Λ} \,,
				\\
				[ (x, λ) \mapsto ψ_λ(x) ]
				&\mapsfrom
				(ψ_λ)_{λ ∈ Λ} \,.
			\end{align*}

	\end{enumerate}
\end{proposition}
