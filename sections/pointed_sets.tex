\chapter{Pointed sets}





\section{Basic notions}



\subsection{Pointed sets}

\begin{definition}
	A \defemph{pointed set} is a pair~$(X, 0_X)$ consisting of a set~$X$ and an element~$0_X$ of~$X$.
	The point~$X$ is the \defemph{underlying set} of~$(X, 0_X)$, and the element~$0_X$ is the \defemph{base point} of~$X$.
\end{definition}

\begin{remark}
	We will often refrain from explicitly mentioning the base point, and simply speak of \enquote{a pointed set~$X$}.
	If we need to access the base point of~$X$, then we will denote it by~$0_X$.
\end{remark}



\subsection{Pointed maps}

\begin{definition}
	Let~$X$ and~$Y$ be two pointed sets.
	A map~$f$ from~$X$ to~$Y$ is a \defemph{pointed} if it preserves basepoints, i.e., if~$f(0_X) = 0_Y$.
\end{definition}

\begin{proposition}
	Let~$X$,~$Y$ and~$Z$ be three pointed sets.
	\begin{enumerate}

		\item
			The identity map of~$X$, denoted by~$\id_X$, is pointed.

		\item
			Let~$f$ be a pointed map from~$X$ to~$Y$ and let~$g$ be a pointed set from~$Y$ to~$Z$.
			The composite~$g ∘ f$ is a map of pointed sets from~$X$ to~$Z$.

	\end{enumerate}
\end{proposition}

\begin{corollary}
	Pointes sets together with the maps of pointed sets between them form a concrete category.
\end{corollary}

\begin{notation}
	We denote the category of pointed sets by~$\Set_*$.
\end{notation}

\begin{remark}
	Since pointed sets form a category, we can now talk about isomorphisms of pointed sets.
	More explicitly, given two pointed sets~$X$ and~$Y$, a pointed map~$f$ from~$X$ to~$Y$ is an \defemph{isomorphism} of pointed sets if there exists a pointed map~$g$ from~$Y$ to~$X$ with~$g ∘ f = \id_X$ and~$f ∘ g = \id_Y$.
\end{remark}

\begin{proposition}
	A pointed map is an isomorphism of pointed sets if and only if it is bijective.
\end{proposition}

\begin{remark}
	We have a forgetful functor from~$\Set_*$ to~$\Set$ that assigns to each pointed set its underlying set.
\end{remark}





\section{Categorical constructions}



\subsection{Pointed subsets}

\begin{definition}
	Let~$X$ be a pointed set.
	A subset of~$X$ is \defemph{pointed} if it contains the basepoint of~$X$.
\end{definition}

\begin{remark}
	Let~$X$ be a pointed set
	\begin{enumerate}

		\item
			Let~$Y$ be a pointed subset of~$X$.
			The set~$Y$ together with the point~$0_X$ becomes again a pointed set.
			We will, in this way, always regard pointed subsets as pointed sets on their own.

		\item
			A pointed set~$Y$ is a pointed subset of~$X$ if and only if~$Y$ is a subset of~$X$ and~$0_Y = 0_X$.

		\item
			A pointed set~$Y$ is a pointed subset of~$X$ if and only if~$Y$ is a subset of~$X$ and the inclusion map from~$Y$ to~$X$ is pointed.

	\end{enumerate}
\end{remark}

\begin{proposition}[Universal property of pointed subsets]
	Let~$X$ be a pointed set, let~$Y$ be a pointed subset of~$X$ and let~$i$ be the inclusion map from~$X$ to~$Y$.
	Let~$Z$ be another pointed set and let~$f$ be a map from~$Z$ to~$Y$.
	The map~$f$ is pointed if and only if the composite~$i ∘ f$ is pointed.
\end{proposition}

\begin{corollary}
	Let~$X$ be a pointed set, let~$Y$ be a pointed subset of~$X$ and let~$i$ be the inclusion map from~$X$ to~$Y$.
	For every pointed set~$Z$, the pointed map~$i$ induces a natural bijection
	\begin{align*}
		\{
			\text{pointed maps~$f \colon Z \to Y$}
		\}
		&\onetoone
		\left\{
			\begin{tabular}{@{}l@{}}
				pointed maps~$g \colon Z \to X$ \\
				with~$g(Z) ⊆ Y$
			\end{tabular}
		\right\} \,,
		\\
		f &\mapsto i ∘ f \,,
		\\
		\restrict{g}{}[Y] &\mapsfrom g \,.
	\end{align*}
\end{corollary}

\begin{proposition}
	Let~$X$ and~$Y$ be two pointed sets and let~$f$ be a pointed map from~$X$ to~$Y$.
	The image of~$f$ is a pointed subset of~$Y$.
\end{proposition}



\subsection{Quotients}

\begin{definition}
	Let~$X$ be a pointed set and let~$∼$ be an equivalence relation on~$X$.
	The pointed set~$(X / {∼}, \class{0_X})$ is the \defemph{quotient of~$X$ by~$∼$}.
\end{definition}

\begin{proposition}
	Let~$X$ be a pointed set and let~$∼$ be an equivalence relation on~$X$.
	The canonical quotient map from~$X$ to~$X / {∼}$ is pointed.
\end{proposition}

\begin{proposition}[Universal property of quotient pointed sets]
	Let~$X$ be a pointed set, let~$∼$ be an equivalence relation on~$X$ and let~$q$ be the canonical quotient map from~$X$ to~$X / {∼}$.
	\begin{enumerate}

		\item
			Let~$Z$ be another pointed set and let~$f$ be a map from~$X / {∼}$ to~$Z$.
			The map~$f$ is pointed if and only of the composite~$f ∘ q$ is pointed.

		\item
			For every pointed set~$Z$, the pointed map~$q$ induces a natural bijection
			\begin{align*}
				\{
					\text{pointed maps~$f \colon X / {∼} \to Z$}
				\}
				&\onetoone
				\left\{
					\begin{tabular}{@{}l@{}}
						pointed maps~$g \colon X \to Z$ \\
						compatible with~$∼$
					\end{tabular}
				\right\}
				\\
				f &\mapsto f ∘ q \,,
				\\
				[ \class{x} \textstyle\mapsto g(x) ] &\mapsfrom g \,.
			\end{align*}

	\end{enumerate}
\end{proposition}



\subsection{Products}

\begin{definition}
	Let~$( X_λ )_{λ ∈ Λ}$ be a family of pointed sets.
	The pointed set consisting of the set~$∏_{λ ∈ Λ} X_λ$ together with the basepoint~$(0_{X_λ})_{λ ∈ Λ}$ is the \defemph{product} of the pointed sets~$X_λ$.
\end{definition}

\begin{proposition}
	Let~$(X_λ)_{λ ∈ Λ}$ be a family of pointed sets.
	For every index~$μ$ in~$Λ$, the canonical projection map from~$∏_{λ ∈ Λ} X_λ$ to~$X_μ$ is pointed.
\end{proposition}

\begin{proposition}[Universal property of the product]
	Let~$(X_λ)_{λ ∈ Λ}$ be a family of pointed sets, and for every index~$μ$ in~$Λ$ let~$p_μ$ be the canonical projection map from~$∏_{λ ∈ Λ} X_λ$ to~$X_μ$.
	\begin{enumerate}

		\item
			Let~$Z$ be another pointed set.
			A map~$f$ from~$Z$ to~$∏_{λ ∈ Λ} X_λ$ is pointed if and only if it is pointed in each coordinate, i.e., if and only if for every index~$μ$ in~$Λ$ the composite~$p_μ ∘ f$ is pointed.

		\item
			The family of pointed maps~$(p_λ)_{λ ∈ Λ}$ induces for every pointed set~$Z$ a natural bijection
			\begin{align*}
				\Biggl\{
					\text{pointed maps~$f \colon Z \to ∏_{λ ∈ Λ} X_λ$}
				\Biggr\}
				&\onetoone
				\left\{
					\begin{tabular}{@{}l@{}}
						families~$(g_λ)_{λ ∈ λ}$ of \\
						pointed maps~$g_λ \colon Z \to X_λ$
					\end{tabular}
				\right\} \,,
				\\
				f
				&\mapsto
				(p_λ ∘ f)_{λ ∈ Λ} \,,
				\\
				[ (x_λ)_{λ ∈ Λ} \textstyle\mapsto (g_λ(x_λ))_{λ ∈ Λ} ]
				&
				\mapsfrom
				(g_λ)_{λ ∈ Λ} \,.
			\end{align*}

	\end{enumerate}
\end{proposition}



\subsection{Pointed sums}

\begin{definition}
	Let~$(X_λ)_{λ ∈ Λ}$ be a family of pointed sets.
	The \defemph{pointed sum} of the pointed sets~$X_λ$, denoted by~$⋁_{λ ∈ Λ} X_λ$, is the following pointed set:
	The underlying set of~$⋁_{λ ∈ Λ} X_λ$ is the quotient set
	\[
		\Biggl( \{ 0 \} ⨿ ∐_{λ ∈ Λ} X_λ \Biggr) \Bigg/ {∼}
	\]
	where~$∼$ is the equivalence relation generated by
	\[
		0 ∼ 0_{X_λ}
		\qquad
		\text{for every~$λ ∈ Λ$}.
	\]
	The basepoint of~$⋁_{λ ∈ Λ} X_λ$ is given by~$\class{0}$.
\end{definition}

\begin{remark}
	One might think about~$⋁_{λ ∈ Λ} X_λ$ as resulting from the pointed sets~$X_λ$ by glueing them together along their base points.
	More explicitly, if the index set~$Λ$ is non-empty, then we could equivalently construct~$⋁_{λ ∈ Λ} X_λ$ as the quotient
	\[
		\Biggl( ∐_{λ ∈ Λ} X_λ \Biggr) \Bigg/ {∼}
	\]
	where the equivalence relation~$∼$ is generated by
	\[
		0_{X_λ} ∼ 0_{X_μ}
		\qquad
		\text{for every~$λ ∈ Λ$}.
	\]
	The basepoint of~$⋁_{λ ∈ Λ} X_λ$ is then given by the equivalence class of~$0_{X_λ}$ for any index~$λ$.

	However, this construction does not work if the index set~$Λ$ is empty, since then the disjoint union~$∐_{λ ∈ Λ} X_λ$ is again empty, and does therefore not admit any choice of basepoint.
	The introduction of the additional point~$0$ in the above definition ensures that we get a pointed set even if~$Λ$ is empty.
\end{remark}

\begin{definition}
	Let~$(X_λ)_{λ ∈ Λ}$ be a family of pointed sets.
	For every index~$μ$ in~$Λ$, the \defemph{canonical inclusion map} from~$X_μ$ to~$⋁_{λ ∈ Λ} X_λ$ is given by the composite
	\[
		X_μ
		\xto{\text{inclusion map}}
		\{ 0 \} ⨿ ∐_{λ ∈ Λ} X_λ
		\xto{\text{quotient map}}
		⋁_{λ ∈ Λ} X_λ \,.
	\]
	The image of an element~$x$ of~$X_μ$ under the canonical inclusion map is denoted by~$[x, μ]$.
\end{definition}

\begin{proposition}
	Let~$(X_λ)_{λ ∈ Λ}$ be a family of pointed sets.
	For every index~$μ$ in~$Λ$, the canonical inclusion map from~$X_μ$ to~$⋁_{λ ∈ Λ} X_λ$ is pointed.
\end{proposition}

\begin{remark}
	The only elements of the sets~$X_λ$ that become identified in~$⋁_{λ ∈ Λ} X_λ$ are the basepoints of the pointed sets~$X_λ$.

	More explicitly, let~$μ$ and~$κ$ be two indices in~$Λ$ and let~$x_μ$ and~$x_κ$ be elements of~$X_μ$ and~$X_κ$ respectively.
	Then~$[x_μ, μ] = [x_κ, κ]$ if and only if both~$x_μ = 0_{X_μ}$ and~$x_κ = 0_{X_κ}$.

	We can in particular regard each of the pointed sets~$X_μ$ as a pointed subset of their pointed sum~$⋁_{λ ∈ Λ} X_λ$ via the canonical inclusion map from~$X_μ$ to~$⋁_{λ ∈ Λ} X_λ$.
\end{remark}

\begin{proposition}[Universal property of the pointed sum]
	Let~$(X_λ)_{λ ∈ Λ}$ be a family of pointed sets and for every index~$μ$ in~$Λ$ let~$i_μ$ be the canonical inclusion map from~$X_μ$ to~$⋁_{λ ∈ Λ} X_λ$.
	\begin{enumerate}

		\item
			Let~$Z$ be another pointed set.
			A map~$f$ from~$⋁_{λ ∈ Λ} X_λ$ to~$Z$ is pointed if and only if the composite~$f ∘ i_λ$ is pointed for every index~$λ$ of~$Λ$.

		\item
			The family of pointed maps~$(i_λ)_{λ ∈ Λ}$ induces a natural bijection
			\begin{align*}
				\Biggl\{
					\text{pointed maps~$f \colon ⋁_{λ ∈ Λ} X_λ \to Z$}
				\Biggr\}
				&\onetoone
				\left\{
					\begin{tabular}{@{}l@{}}
						families~$(g_λ)_{λ ∈ Λ}$ of \\
						pointed maps~$g_λ \colon X_λ \to Z$
					\end{tabular}
				\right\}
				\\
				f
				&\mapsto
				(f ∘ i_λ)_{λ ∈ Λ} \,,
				\\
				\left[
					y \mapsto
					\begin{cases*}
						g_λ(x) & if~$y = [x, λ]$ with~$x ∈ X_λ$, \\
						0_Z    & if~$y = \class{0}$,
					\end{cases*}
				\right]
				&\mapsfrom
				(g_λ)_{λ ∈ Λ}
			\end{align*}
	\end{enumerate}
\end{proposition}

\begin{remark}
	Let~$(X_λ)_{λ ∈ Λ}$ be a family of pointed sets.
	The maps~$f_{λμ}$ from~$X_λ$ to~$X_μ$ given by
	\[
		f_{λμ}
		≔
		\begin{cases*}
			\id_{X_λ} & if~$λ = μ$, \\
			0         & otherwise,
		\end{cases*}
	\]
	are pointed, and therefore assemble into a pointed map
	\[
		f
		\colon
		⋁_{λ ∈ Λ} X_λ
		\to
		∏_{λ ∈ Λ} X_λ \,.
	\]
	The map~$f$ is given on elements by
	\[
		f( [x, λ] )
		=
		(x_μ)_{μ ∈ Λ}
		\quad\text{with}\quad
		x_μ
		=
		\begin{cases*}
			x & if~$μ = λ$, \\
			0 & otherwise.
		\end{cases*}
	\]
	The image of~$X_λ$ under the map~$f$ is hence the coordinate axis
	\[
		X'_λ
		≔
		\{
			(x_μ)_μ
		\suchthat
			\text{$x_μ = 0$ whenever~$μ ≠ λ$}
		\} \,.
	\]
	We find from this explicit description of the pointed map~$f$ that it is injective.
	We can therefore identify the pointed sum~$⋁_{λ ∈ Λ} X_λ$ with its image in~$∏_{λ ∈ Λ} X_λ$.

	This identification is sometimes used to \emph{define} the pointed sum~$⋁_{λ ∈ Λ} X_λ$ as a pointed subset of the product~$∏_{λ ∈ Λ} X_λ$.
\end{remark}



\subsection{Free pointed sets}

\begin{definition}
	Let~$X$ be a set.
	The \defemph{free pointed set} on~$X$ is the pointed set~$X^+$ as follows:
	the underlying set of~$X$ is
	\[
		X ⨿ \{ 0 \} \,,
	\]
	and the basepoint of~$X^+$ is the additional point~$0$.
	The inclusion map from~$X$ to~$X^+$ is the \defemph{canonical map} from~$X$ to~$X^+$.
\end{definition}

\begin{proposition}[Universal property of the free pointed set]
	\label{universal_property_of_free_pointed_sets}
	Let~$X$ be a set and let~$i$ be the canonical map from~$X$ to~$X^+$.

	\begin{enumerate}

		\item
			Let~$Y$ be a pointed set.
			Every map from~$X$ to (the underlying set of)~$Y$ extends uniquely along~$i$ to a pointed map from~$X^+$ to~$Y$.

		\item
			The map~$i$ induces for every pointed set~$Y$ a natural bijection
			\begin{align*}
				\{
					\text{maps~$f \colon X \to Y$}
				\}
				&\onetoone
				\{
					\text{pointed maps~$g \colon X^+ \to Y$}
				\} \,,
				\\
				f
				&\mapsto
				\left[
					x^+
					\mapsto
					\begin{cases*}
						g(x^+) & if~$x^+ ∈ X$, \\
						0_Y    & if~$x^+ = 0$,
					\end{cases*}
				\right] \,,
				\\
				g ∘ i
				&\mapsfrom
				g \,.
			\end{align*}

	\end{enumerate}
\end{proposition}

\begin{proposition}[Functoriality of free pointed sets]
	Let~$X$,~$Y$ and~$Z$ be sets.
	\begin{enumerate}

		\item
			Let~$X$ and~$Y$ be two sets and let~$f$ be a map from~$X$ to~$Y$.
			There exists a unique pointed map~$f^+$ from~$X^+$ to~$Y^+$ that makes the diagram
			\[
				\begin{tikzcd}
					X^+
					\arrow{r}[above]{f^+}
					&
					Y^+
					\\
					X
					\arrow{r}[above]{f}
					\arrow{u}
					&
					Y
					\arrow{u}
				\end{tikzcd}
			\]
			commute, where the vertical arrows are given by the canonical maps.
			This pointed map is given by~$f^+(x) = f(x)$ for every element~$x$ of~$X$ and~$f(0) = 0$.

		\item
			We have~$(\id_X)^+ = \id_{X^+}$.

		\item
			Let~$f$ be a map from~$X$ to~$Y$ and let~$g$ be a map from~$Y$ to~$Z$.
			We have~$(g ∘ f)^+ = g^+ ∘ f^+$.

	\end{enumerate}
\end{proposition}

\begin{remark}
	We have thus constructed a functor~$(\ph)^+$ from~$\Set$ to~$\Set_*$, and find from \cref{universal_property_of_free_pointed_sets} that this functor is left-adjoint to the forgetful functor from~$\Set_*$ to~$\Set$.
\end{remark}





\section{Monoidal structure}



\subsection{Multipointed maps}

\subsubsection{Maps in one variable}

\begin{definition}
	Let~$X$ be a set and let~$Y$ be a pointed set.
	\begin{enumerate}

		\item
			The map from~$X$ to~$Y$ with constant value~$0_Y$ is the \defemph{zero map} from~$X$ to~$Y$.

		\item
			The pointed set~$\eMap(X, Y)$ is given as follows:
			the underlying set of~$\eMap(X, Y)$ is the set of all maps from~$X$ to~$Y$, and the basepoint of~$\eMap(X, Y)$ is given by the zero map from~$X$ to~$Y$.

	\end{enumerate}
\end{definition}

\begin{proposition}
	Let~$X$ and~$Y$ be two pointed sets.
	The zero map from~$X$ to~$Y$ is pointed.
\end{proposition}

\begin{corollary}
	Let~$X$ and~$Y$ be two pointed set.
	The set of pointed maps from~$X$ to~$Y$ is a pointed subset of~$\eMap(X, Y)$.
\end{corollary}

\begin{notation}
	Let~$X$ and~$Y$ be two pointed spaces.
	The pointed set of pointed maps from~$X$ to~$Y$ is denoted by~$\eHom(X, Y)$.
\end{notation}

\begin{remark}
	The underlying set of~$\eHom(X, Y)$ is given by~$\Hom(X, Y)$.
\end{remark}

\subsubsection{Maps in multiple variables}

\begin{definition}
	Let~$X_1, \dotsc, X_n$ and~$Y$ be pointed sets.
	A map~$h$ from~$X_1 × \dotsb × X_n$ to~$Y$ is \defemph{multipointed} if
	\[
		h(x_1, \dotsc, x_n) = 0
	\]
	whenever~$x_i = 0$ for some index~$i = 1, \dotsc, n$.
	The set of pointed maps from~$X_1 × \dotsb × X_n$ to~$Y$ is denoted by~$\Pt(X_1, \dotsc, X_n; Y)$.
\end{definition}

\begin{proposition}
	Let~$X_1, \dotsc, X_n$ and~$Y$ be pointed sets.
	The zero map from~$X_1 × \dotsb × X_n$ to~$Y$ is multipointed.
\end{proposition}

\begin{corollary}
	Let~$X_1, \dotsc, X_n$ and~$Y$ be pointed sets.
	The set~$\Pt(X_1, \dotsc, X_n; Y)$ is a pointed subset of~$\eMap(X_1 × \dotsb × X_n, Y)$.
\end{corollary}

\begin{notation}
	Let~$X_1, \dotsc, X_n$ and~$Y$ be pointed sets.
	The pointed set of multipointed maps from~$X_1 × \dotsb × X_n$ to~$Y$ is denoted by~$\ePt(X_1, \dotsc, X_n; Y)$.
\end{notation}

\begin{remark}
	Let~$X_1, \dotsc, X_n$ and~$Y$ be pointed sets.
	The underlying set of~$\ePt(X_1, \dotsc, X_n; Y)$ is~$\Pt(X_1, \dotsc, X_n; Y)$.
\end{remark}

\begin{remark}
	We have~$\eHom(X, Y) = \ePt(X; Y)$ for any two pointed sets~$X$ and~$Y$.
\end{remark}

\begin{proposition}
	Let~$X_1, \dotsc, X_n$ and~$Y$ be pointed sets.
	The currying bijection
	\[
		\Map(X_1 × \dotsb × X_n, Y)
		≅
		\Map(X_1, \Map(X_2 × \dotsb × X_n, Y))
	\]
	restricts to a natural isomorphism of pointed sets
	\[
		\ePt(X_1, \dotsc, X_n; Y)
		≅
		\ePt(X_1, \ePt(X_2, \dotsc, X_n; Y)) \,.
	\]
\end{proposition}



\subsection{Smash products}

\begin{definition}
	Let~$X_1, \dotsc, X_n$ be pointed sets.
	The \defemph{smash product} of~$X_1, \dotsc, X_n$ is the pointed set~$X_1 ⨳ \dotsb ⨳ X_n$ given is follows:
	The underlying set of~$X_1 ⨳ \dotsb ⨳ X_n$ is the quotient
	\[
		\Bigl( \{ 0 \} ⨿ (X_1 × \dotsb × X_n) \Bigr) \Big/ {∼}
	\]
	where the equivalence relation~$∼$ is generated by
	\[
		0 ∼ (x_1, \dotsc, x_n)
		\qquad
		\text{whenever~$x_i = 0_{X_i}$ for some index~$i$}.
	\]
	The basepoint of~$X_1 ⨳ \dotsb ⨳ X_n$ is~$\class{0}$.
	The \defemph{canonical map} from~$X_1 × \dotsb × X_n$ to~$X_1 ⨳ \dotsb ⨳ X_n$ is the composite
	\[
		X_1 × \dotsb × X_n
		\xto{\text{inclusion map}}
		\{ 0 \} ⨿ (X_1 × \dotsb × X_n)
		\xto{\text{quotient map}}
		X_1 ⨳ \dotsb ⨳ X_n \,.
	\]
	The image of an element~$(x_1, \dotsc, x_n)$ of~$X_1 × \dotsb × X_n$ in~$X_1 ⨳ \dotsb ⨳ X_n$ is denoted by
	\[
		x_1 ⨳ \dotsb ⨳ x_n \,.
	\]
\end{definition}

\begin{proposition}
	Let~$X_1, \dotsc, X_n$ be pointed sets.
	The canonical map
	\[
		X_1 × \dotsb × X_n
		\to
		X_1 ⨳ \dotsb ⨳ X_n \,,
		\quad
		(x_1, \dotsc, x_n)
		\mapsto
		x_1 ⨳ \dotsb ⨳ x_n
	\]
	is multipointed.
\end{proposition}

\begin{proposition}[Universal property of the smash product]
	Let~$X_1, \dotsc, X_n$ be pointed sets and let~$c$ be the canonical map from~$X_1 × \dotsb × X_n$ to~$X_1 ⨳ \dotsb ⨳ X_n$.
	\begin{enumerate}

		\item
			Let~$Y$ be another pointed set.
			A map~$h$ from~$X_1 ⨳ \dotsb ⨳ X_n$ to~$Y$ is pointed if and only if the composite~$h ∘ c$ is multipointed.

		\item
			The map~$c$ induces for every pointed set~$Y$ a natural bijection
			\begin{align*}
				\left\{
					\begin{tabular}{@{}l@{}}
						pointed maps \\
						$h \colon X_1 ⨳ \dotsb ⨳ X_n \to Y$
					\end{tabular}
				\right\}
				&\onetoone
				\left\{
					\begin{tabular}{@{}l@{}}
						multipointed maps \\
						$k \colon X_1 × \dotsb × X_n \to Y$
					\end{tabular}
				\right\} \,,
				\\
				h
				&\mapsto
				h ∘ c \,,
				\\
				[ x_1 ⨳ \dotsb ⨳ x_n \mapsto k(x_1, \dotsc, x_n) ]
				&\mapsfrom
				k \,.
			\end{align*}

	\end{enumerate}
\end{proposition}

\begin{proposition}[Functoriality of the smash product]
	\label{functoriality_of_smash_product}
	Let~$X_1, \dotsc, X_n$ and~$Y_1, \dotsc, Y_n$ be pointed sets, and let
	\[
		f_1 \colon X_1 \to Y_1 \,,
		\quad \,,
		f_n \colon X_n \to Y_n
	\]
	be pointed maps.
	There exists a unique, well-defined pointed map from~$X_1 ⨳ \dotsb ⨳ X_n$ to~$Y_1 ⨳ \dotsb ⨳ Y_ni$ such that for all elements~$x_i$ of~$X_i$ with~$i = 1, \dotsc, n$,
	\[
		x_1 ⨳ \dotsb ⨳ x_n
		\mapsto
		f_1(x_1) ⨳ \dotsb ⨳ f_n(x_n) \,.
	\]
\end{proposition}

\begin{notation}
	In the situation of \cref{functoriality_of_smash_product}, the induces pointed map from~$X_1 ⨳ \dotsb ⨳ X_n$ to~$Y_1 ⨳ \dotsb Y_n$ is denoted by
	\[
		f_1 ⨳ \dotsb ⨳ f_n \,.
	\]
\end{notation}

\begin{proposition}[Functoriality of the smash product (continued)]
	Let~$X_1, \dotsc, X_n$,~$Y_1, \dotsc, Y_n$ and~$Z_1, \dotsc, Z_n$ be pointed sets.
	\begin{enumerate}

		\item
			We have~$\id_{X_1} ⨳ \dotsb ⨳ \id_{X_n} = \id_{X_1 ⨳ \dotsb ⨳ X_n}$.

		\item
			For all pointed maps
			\[
				f_1 \colon X_1 \to Y_1 \,,
				\quad
				\dotsc \,,
				\quad
				f_n \colon X_n \to Y_n \,,
				\qquad
				g_1 \colon Y_1 \to Z_1 \,,
				\quad
				\dotsc \,,
				\quad
				g_n \colon Y_n \to Z_n
			\]
			we have
			\[
				(g_1 ⨳ \dotsb ⨳ g_n) ∘ (f_1 ⨳ \dotsb ⨳ f_n)
				=
				(g_1 ∘ f_1) ⨳ \dotsb ⨳ (g_n ∘ f_n) \,.
			\]

	\end{enumerate}
\end{proposition}

\begin{definition}
	The pointed set~$\One$ is given by the set~$\{0, 1\}$ and basepoint~$0$.
\end{definition}

\begin{proposition}[Unit for the smash product]
	Let~$X$ be a pointed set.
	The two maps
	\[
		\One ⨳ X \to X \,,
		\quad
		1 ⨳ x \mapsto x \,,
		\quad
		0 ⨳ x \mapsto 0
	\]
	and
	\[
		X ⨳ \One \to X \,,
		\quad
		x ⨳ 1 \mapsto x \,,
		\quad
		x ⨳ 0 \mapsto 0
	\]
	are well-defined, natural isomorphisms of pointed sets.
\end{proposition}

\begin{proposition}[Associativity of the smash product]
	Let~$X_{i, j}$ with~$i = 1, \dotsc, k$ and~$j = 1, \dotsc, n_i$ be pointed sets.
	The map
	\begin{align*}
		(X_{1, 1} ⨳ \dotsb ⨳ X_{1, n_1})
		⨳ \dotsb ⨳
		(X_{k, 1} ⨳ \dotsb ⨳ X_{k, n_k})
		&\to
		X_{1, 1} ⨳ \dotsb ⨳ X_{1, n_1}
		⨳ \dotsb ⨳
		X_{k, 1} ⨳ \dotsb ⨳ X_{k, n_k}
		\\
		(x_{1, 1} ⨳ \dotsb ⨳ x_{1, n_1})
		⨳ \dotsb ⨳
		(x_{k, 1} ⨳ \dotsb ⨳ x_{k, n_k})
		&\mapsto
		x_{1, 1} ⨳ \dotsb ⨳ x_{1, n_1}
		⨳ \dotsb ⨳
		x_{k, 1} ⨳ \dotsb ⨳ x_{k, n_k}
	\end{align*}
	is a well-defined, natural isomorphism of pointed sets.
\end{proposition}

\begin{proposition}[Commutativity of the smash product]
	Let~$X_1, \dotsc, X_n$ be pointed sets and let~$σ$ be a permutation on the index set~$\{1, \dotsc, n\}$, i.e., an element of the symmetric group~$\symm_n$.
	The map
	\[
		X_1 ⨳ \dotsb ⨳ X_n
		\to
		X_{σ(1)} ⨳ \dotsb ⨳ X_{σ(n)} \,,
		\quad
		x_1 ⨳ \dotsb ⨳ x_n
		\mapsto
		x_{σ(1)} ⨳ \dotsb ⨳ x_{σ(n)}
	\]
	is a well-defined, natural isomorphism of pointed sets.
\end{proposition}

\begin{proposition}[Distributivity of the smash product]
	Let~$X_{λ ∈ Λ}$ be a family of pointed sets and let~$Y$ be another pointed set.
	The two maps
	\[
		\Biggl( ⋁_{λ ∈ Λ} X_λ \Biggr) ⨳ Y
		\to
		⋁_{λ ∈ Λ} (X_λ ⨳ Y) \,,
		\quad
		[x, λ] ⨳ y
		\mapsto
		[x ⨳ y, λ]
	\]
	and
	\[
		Y ⨳ \Biggl( ⋁_{λ ∈ Λ} X_λ \Biggr)
		\to
		⋁_{λ ∈ Λ} (Y ⨳ X_λ) \,,
		\quad
		y ⨳ [x, λ]
		\mapsto
		[y ⨳ x, λ]
	\]
	are a well-defined, natural isomorphisms of pointed sets.
\end{proposition}



\subsection{Enriched isomorphisms}

\begin{proposition}
	Let~$(X_λ)_{λ ∈ Λ}$ be a family of pointed sets and let~$Y$ be another pointed set.
	For every index~$μ$ in~$Λ$ let~$i_μ$ denote the canonical inclusion map from~$X_μ$ to~$⋁_{λ ∈ Λ} X_λ$.
	The natural isomorphisms of sets
	\[
		\Hom\Biggl( ⋁_{λ ∈ Λ} X_λ, Y \Biggr)
		\to
		∏_{λ ∈ Λ} \Hom(X_λ, Y) \,,
		\quad
		f
		\mapsto
		(f ∘ i_λ)_{λ ∈ Λ}
	\]
	is already a natural isomorphism of pointed sets
	\[
		\eHom\Biggl( ⋁_{λ ∈ Λ} X_λ, Y \Biggr)
		\to
		∏_{λ ∈ Λ} \eHom(X_λ, Y) \,.
	\]
\end{proposition}

\begin{proposition}
	Let~$X$ be a pointed set and let~$(Y_λ)_{λ ∈ Λ}$ be a family of pointed sets.
	For every index~$μ$ in~$Λ$ let~$p_μ$ be the canonical projection map from~$∏_{λ ∈ Λ} Y_λ$ to~$Y_μ$.
	The natural isomorphism of sets
	\[
		\Hom\Biggl( X, ∏_{λ ∈ Λ} Y_λ \Biggr)
		\to
		∏_{λ ∈ Λ} \Hom(X, Y_λ) \,,
		\quad
		f
		\mapsto
		(p_λ ∘ f)_{λ ∈ Λ}
	\]
	is already a natural isomorphism of pointed sets
	\[
		\eHom\Biggl( X, ∏_{λ ∈ Λ} Y_λ \Biggr)
		\to
		∏_{λ ∈ Λ} \eHom(X, Y_λ) \,.
	\]
\end{proposition}

\begin{proposition}
	Let~$X_1, \dotsc, X_n$ and~$Y$ be pointed sets and let~$c$ be the canonical map from the set-theoretic product~$X_1 × \dotsb × X_n$ into the smash product~$X_1 ⨳ \dotsb ⨳ X_n$.
	The natural bijection of sets
	\[
		\Hom(X_1 ⨳ \dotsb ⨳ X_n, Y)
		\to
		\Pt(X_1, \dotsc, X_n; Y) \,,
		\quad
		h
		\mapsto
		h ∘ c
	\]
	is already a natural isomorphism of pointed sets
	\[
		\eHom(X_1 ⨳ \dotsb ⨳ X_n, Y)
		\to
		\ePt(X_1, \dotsc, X_n; Y) \,.
	\]
\end{proposition}

\begin{remark}
	We have in particular for any three pointed sets~$X$,~$Y$ and~$Z$ the natural isomorphism of pointed sets
	\[
		\eHom(X ⨳ Y, Z) ≅ \eHom(X, \eHom(Y, Z)) \,.
	\]
\end{remark}
